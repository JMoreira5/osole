<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\AdminUser;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin.user');
    }
    
    public function index()
    {
    	$users = AdminUser::all();

        return view("admin-home")->with('users', $users);
    }

    public function create()
    {
        return view('admin-create');
    }

    public function destroy($id)
    {
        $user = AdminUser::findOrFail($id);
        $user->delete();

        return redirect()->route('admin.users.list');
    }

    public function edit($id)
    {
        $user = AdminUser::findOrFail($id);

        return view('admin-edit')->with('user', $user);
    }

    public function update(Request $request, $id)
    {
        $user = AdminUser::findOrFail($id);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        return redirect()->route('admin.users.list');
    }

    public function store(Request $request)
    {
        $user = new AdminUser($request->all());
        $user->save();

        return redirect()->route('admin.users.list');
    }

}