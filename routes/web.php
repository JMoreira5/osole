<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

// Backend routes
Route::get('admin_login', 'AdminAuth\LoginController@showLoginForm');
Route::post('admin_login', 'AdminAuth\LoginController@login');

Route::post('admin_logout', 'AdminAuth\LoginController@logout');

Route::post('admin_password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail');
Route::get('admin_password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm');
Route::post('admin_password/reset', 'AdminAuth\ResetPasswordController@reset');
Route::get('admin_password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');

Route::get('admin/', [
	'as' => 'admin.users.list',
	'uses' => 'AdminController@index'
]);

Route::get('admin/usuarios/accesos', [
	'as' => 'admin.users.access',
	'uses' => 'AdminController@access'
]);

Route::get('admin/usuario/{id}/eliminar',[
	'as' => 'admin.users.destroy',
	'uses' => 'AdminController@destroy'
]);

Route::get('admin/usuario/crear',[
	'as' => 'admin.users.create', 
	'uses' => 'AdminController@create'
]);

Route::get('admin/usuario/{id}/editar',[
	'as' => 'admin.users.edit', 
	'uses' => 'AdminController@edit'
]);

Route::put('admin/usuario/{id}/actualizar',[
	'as' => 'admin.users.update',
	'uses' => 'AdminController@update'
]);

Route::post('admin/usuario/guardar',[
	'as' => 'admin.users.store',
	'uses' => 'AdminController@store'
]);

// Frontend routes
Route::get('home', 'HomeController@index');