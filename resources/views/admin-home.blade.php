@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="col-sm-offset-2 col-sm-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Usuarios Registrados
                    </div>
                    <div class="panel-body">
                          <a type="button" href="{{ route('admin.users.create') }}" class="btn btn-success">Crear usuario</a>

                        <table class="table table-striped user-table">
                            <thead>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Acciones</th>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{ $user->name }}</td>  
                                        <td>{{ $user->email }}</td> 
                                        <td>
                                            <a href="{{ route('admin.users.destroy', $user->id) }}" onclick="return confirm('¿Seguro que  desea eliminar este Usuario?')" class="btn btn-danger"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                                            <a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-warning"><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
