@extends('layouts.app')

@section('title', 'Edici&oacute;n de $user->name')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="col-sm-offset-2 col-sm-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Editar usuario <b>{{ $user->name }}</b>
                    </div>
                    <div class="panel-body">

						{!! Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'PUT']) !!}

							<div class="form-group">
								{!! Form::label('name', 'Name') !!}
								{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'First Name', 'required']) !!}
							</div>
							<div class="form-group">
								{!! Form::label('email', 'E-Mail') !!}
								{!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'E-Mail', 'required']) !!}
							</div>
							<div class="form-group">
								{!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
							</div>

						{!! Form::close() !!}


					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection